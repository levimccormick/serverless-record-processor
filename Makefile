build:
	docker run --rm -it \
		-v `pwd`/code:/code:ro \
		-v `pwd`/output:/output \
		package-lambda
		
	aws s3 cp output/ s3://baseinfra-sourcebucket-1oedthneccqbl/serverless-record-processing/lambda/ --recursive

validate:
	aws cloudformation validate-template --template-body file://infra/processing-table.yml
	
	
create:
	aws cloudformation create-stack \
		--stack-name srp-processing-table \
		--template-body file://infra/processing-table.yml \
		--parameters \
			ParameterKey=ProjectKey,ParameterValue=srp \
		--timeout-in-minutes 30 \
		--capabilities CAPABILITY_NAMED_IAM \
		--on-failure DELETE \
		--tags Key=project,Value=srp