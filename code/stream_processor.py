import os
import json
import boto3
from boto3.dynamodb.types import TypeDeserializer
from botocore.exceptions import ClientError

ddb = boto3.client("dynamodb")
s3 = boto3.client('s3')

def handler(event, context):
    
    # Start with latest {event} from DDB
    record_id = TypeDeserializer(event['Records'][0])['dynamodb']['NewImage']['record_id']
    
    # Pull the previous >200 events from DDB
    response = ddb.query(
        TableName=os.getenv('TABLE_NAME'),
        IndexName=os.getenv('INDEX_NAME'),
        Select='ALL_PROJECTED_ATTRIBUTES',
        Limit=200,
        ScanIndexForward=True,
        KeyConditionExpression="partitionKeyName=:partitionkeyval"
    )
    
    # Mock up the processing
    output_record = {}
    # TODO
    for record in response['Items']:
        print(f"Record date: {record['date']}")
     
    
    
    # Write the result to S3
    response = s3.put_object(
        Body=json.dumps(output_record), # string or bytes object?
        Bucket=os.getenv('BUCKET_NAME'), 
        Key='' # bucket path format?
        )
    
# def test_handler():
#     test_event = """\
#     {
#       "Records":[
#           {
#              "eventID":"1",
#              "eventName":"INSERT",
#              "eventVersion":"1.0",
#              "eventSource":"aws:dynamodb",
#              "awsRegion":"us-east-1",
#              "dynamodb":{
#                 "Keys":{
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "NewImage":{
#                   "Message":{
#                       "S":"New item!"
#                   },
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "SequenceNumber":"111",
#                 "SizeBytes":26,
#                 "StreamViewType":"NEW_AND_OLD_IMAGES"
#              },
#              "eventSourceARN":"stream-ARN"
#           },
#           {
#              "eventID":"2",
#              "eventName":"MODIFY",
#              "eventVersion":"1.0",
#              "eventSource":"aws:dynamodb",
#              "awsRegion":"us-east-1",
#              "dynamodb":{
#                 "Keys":{
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "NewImage":{
#                   "Message":{
#                       "S":"This item has changed"
#                   },
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "OldImage":{
#                   "Message":{
#                       "S":"New item!"
#                   },
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "SequenceNumber":"222",
#                 "SizeBytes":59,
#                 "StreamViewType":"NEW_AND_OLD_IMAGES"
#              },
#              "eventSourceARN":"stream-ARN"
#           },
#           {
#              "eventID":"3",
#              "eventName":"REMOVE",
#              "eventVersion":"1.0",
#              "eventSource":"aws:dynamodb",
#              "awsRegion":"us-east-1",
#              "dynamodb":{
#                 "Keys":{
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "OldImage":{
#                   "Message":{
#                       "S":"This item has changed"
#                   },
#                   "Id":{
#                       "N":"101"
#                   }
#                 },
#                 "SequenceNumber":"333",
#                 "SizeBytes":38,
#                 "StreamViewType":"NEW_AND_OLD_IMAGES"
#              },
#              "eventSourceARN":"stream-ARN"
#           }
#       ]
#     }\
#     """